from __future__ import print_function #I'm using 3.x style print
import pandas as pd

import time
import datetime
from ib.opt import Connection, message
from ib.ext.Contract import Contract
from ib.ext.TickType import TickType as tt
from time import sleep

params = {
        'host': '127.0.0.1',
        'port': 4001,
        'clientId': 999,  
        'notifyall': False,
        '_debug': False,
        'reconnect': 3,  # -1 forever, 0 No, > 0 number of retries
        'timeout': 3.0,  # timeout between reconnections
        'timeoffset': True,  # Use offset to server for timestamps if needed
        'timerefresh': 60.0,  # How often to refresh the timeoffset
        'file_path' : '/home/gmanser/mktData/IBdata_' +str(datetime.datetime.now().date())+ '.csv',
        'universe' : 'universeIB.csv'
    }

def error_handler(msg):
    print (msg)

def level1MktDataHandler(msg):
    if msg.field in [ tt.BID, tt.BID_SIZE, tt.ASK, tt.ASK_SIZE, tt.VOLUME, tt.LAST]:
       
        if msg.field == tt.BID or msg.field == tt.ASK or msg.field == tt.LAST:
            contracts.loc[msg.tickerId,tt.getField(msg.field)] = msg.price
        elif msg.field == tt.BID_SIZE or msg.field == tt.ASK_SIZE or msg.field == tt.VOLUME :
            contracts.loc[msg.tickerId,tt.getField(msg.field)] = msg.size
        if msg.field == tt.LAST:
            print(contracts.loc[msg.tickerId,'m_symbol'],msg.price)
            
def extract_time_from_currentTime_message(msg):
    print (msg.time)
    current_exchange_time = msg.time
    print('current exchange time : {}'.format(current_exchange_time))
    return current_exchange_time
            
def create_ib_connection( port, ID):
        """Connect to IB Gateway """
        conn = Connection.create(port=port, clientId=ID)
        conn.register(level1MktDataHandler, message.tickPrice, message.tickSize, message.tickString())
        conn.register(error_handler, 'Error')
        conn.register(extract_time_from_currentTime_message, 'CurrentTime')
        conn.connect()
        return conn
    
def killRequest(contracts, connection):
    for i in contracts.index:
        connection.cancelMktData(i)
    
    connection.disconnect()
    print('Connection Closed')
    sleep(5)

def create_contract_dataframe(instruments_file):
    """Create contract dataframe """
    contracts = pd.DataFrame.from_csv(instruments_file)
    contracts = contracts.sort('m_expiry').groupby('m_symbol').head(2)
    contracts.columns = ['m_currency', 'm_exchange', 'm_expiry','m_includeExpired',
 'm_localSymbol', 'm_multiplier', 'm_primaryExch', 'm_right', 'm_secType', 'm_strike',
 'm_symbol', 'm_tradingClass']
    
    contracts = contracts[['m_symbol', 'm_secType', 'm_exchange','m_currency', 'm_expiry','m_multiplier']]
    contracts['bidPrice'] = 0
    contracts['bidSize'] = 0
    contracts['askPrice'] = 0
    contracts['askSize'] = 0
    contracts['volume'] =0
    contracts['lastPrice'] = 0
    contracts['timestamp']=0
    contracts.index.name = 'Id'
    
    return contracts


conn = create_ib_connection(params['port'],params['clientId'])

contracts = create_contract_dataframe(params['universe'])

contracts.to_csv(params['file_path'], mode='a')

for index, row in contracts.iterrows():
    c = Contract()
    c.m_symbol = row['m_symbol']
    c.m_exchange = row['m_exchange']
    c.m_currency = row['m_currency']
    c.m_secType = row['m_secType']
    c.m_expiry = row['m_expiry']
    c.m_multiplier = row['m_multiplier']
    conn.reqMktData(str(index),c,"",False)
    conn.reqCurrentTime()
    sleep(0.1)
    
    
endTime = datetime.datetime.now() + datetime.timedelta(minutes=1380)
while True:
    if datetime.datetime.now() >= endTime:
        break
    
    contracts['timestamp'] = time.time()
    contracts.to_csv(params['file_path'], mode='a', header=False)
    sleep(1)

killRequest(contracts, conn)